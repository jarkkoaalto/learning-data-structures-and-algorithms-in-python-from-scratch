

class BinarySearchTree:
    class _Node:
        __slots__ = "_element","_left","_right"

        def __init__(self,element, left=None, right=None):
            self._element = element
            self._left = left
            self._right = right

    def __init__(self):
        self._root = None
        self._size = 0

    def insert(self,e):
        troot = self._root
        ttroot = None
        while troot:
            ttroot = troot
            if e < troot._element:
                troot = troot._left
            elif e > troot._element:
                troot = troot._right
        node = self._root
        if self._root:
            if e <ttroot._element:
                ttroot._left = node
            else:
                ttroot._right = node
    def search(self,k):
        troot = self._root
        while troot:
            if k < troot._element:
                troot = troot._left
            elif k > troot._element:
                troot = troot._right
            else:
                return True
        return False

    def levelorder(self):
        if troot:
            self.inorder(troot._left)
            print(troot._element, end="--")
            self.inorder(troot._right)

    def preorder(self,troot):
        if troot:
            print(troot._element, end='--')
            self.preorder(troot._left)
            self.preorder(troot._right)

    def postorder(self,troot):
        if troot:
            self.postorder(troot._left)
            self.postorder(troot._right)
            print(troot._element, end='--')

B = BinarySearchTree()
B.insert(70)
B.insert(30)
B.insert(90)
B.insert(50)
B.insert(110)


