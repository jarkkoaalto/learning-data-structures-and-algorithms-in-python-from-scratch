from section12-LinkedList import 

class BinaryTree:
    class _Node:
        __slots__ = '_element','_left','_right'

        def __init__(self, element, left=None, right=None):
            self._element = element
            self._left = left
            self._right = right

    def __init__(self):
        self._root = None
        self._size = 0

    def maketree(self,e,left,rigth):
        self._root = self._Node(e,left._root, rigth._root)
        left._root = None
        rigth._root = None

    def levelorder(self):
        Q=LinkedQueue()
        t=self._root
        print(t._element, end="--")
        Q.enqueue(t)

        while not Q.is_empty():
            t=Q.dequeue()
            if t._left:
                print(t._left._element, end="--")
                Q.enqueue(t._left)
            if t._right:
                print(t._right._element, end="--")
                Q.enqueue(t._element)

    def inorder(self, troot):
        if troot:
            self.inorder(troot._left)
            print(troot._element, end="--")
            self.inorder(troot._right)

    def preorder(self, troot):
        if troot:
            print(troot._element, end='--')
            self.preorder(troot._left)
            self.preorder(troot._right)

    def postorder(self, troot):
        if troot:
            self.postorder(troot._left)
            self.postorder(troot._right)
            print(troot._element, end="--")


a = BinaryTree()
x = BinaryTree()
y = BinaryTree()
z = BinaryTree()
r = BinaryTree()
s = BinaryTree()
t = BinaryTree()

x.maketree(40,a,a)
y.maketree(60,a,a)
z.maketree(70,a,a)
r.maketree(20,x,a)
s.maketree(40,r,a)
t.maketree(10,z,s)

t.levelorder()
print()
t.preorder(t._root)
print()
t.inorder(t._root)
print()
t.postorder(t._root)
print()