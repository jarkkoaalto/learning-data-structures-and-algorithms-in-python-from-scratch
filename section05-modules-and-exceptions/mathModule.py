import math

print(dir(math))

l = dir(math)
for i in l:
    print(i)

# Using math library another way
print(math.pi)
print(math.e)
print(math.pow(2,4))

# or 
import math as m
print (m.pow(5,6))

# or 
from math import pi as p
print(p)

print(math.sqrt(25))