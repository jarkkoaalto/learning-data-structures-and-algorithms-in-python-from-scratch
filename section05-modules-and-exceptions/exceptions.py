l = [10,12,14,16]

try:
   # print(l[10]) -> list index out of range
   print(l[2]) # 14
except IndexError as e:
    print(e)

print(l) # [10,12,14,16]



