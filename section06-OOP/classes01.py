class Student:
    def __init__(self):
        self.name = 'abc'
        self.roll = 101
        self.marks = 78.35
        print('In Constructor')

    def display(self):
        print(self.name, self.roll, self.marks)

S = Student()
S.__init__()