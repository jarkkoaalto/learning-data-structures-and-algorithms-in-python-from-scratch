class Student:
    def __init__(self,name,roll,marks):
        self.name = name
        self.roll = roll
        self.marks = marks 
        # Student.collage='UCLA' # Static variable inside class

    def display(self):
        print("Student Name:", self.name)
        print("Student Roll no: ", self.roll)
        print('Student Marks',self.marks)
        college = "UCLA" ## Local variable
        print('College Name:', college) # Local variable
        ##Student.collage = 'MIT'
        #print('Collage Name:',Student.college)
        print()   

# Student.collage = 'MIT' #Static variable outside class. This must be declare before call
S1 = Student('aaa',101,60.44)
S2 = Student('bbb',102,55.11)
S1.display()
S2.display()