class Student:
    def __init__(self, name, roll, marks):
        self.name = name
        self.roll = roll
        self.marks = marks
    
    def display(self):
        print('Student name:',self.name)
        print("Student Roll No:",self.roll)
        print("Student marks:", self.marks)
        print('')

S=Student('aaa',101,34.56)
S.display()