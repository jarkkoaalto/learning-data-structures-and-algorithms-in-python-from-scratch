class College:
    def __init__(self):
        print('Outher Class Constructor')
    
    class Student:
        def __init__(self):
            print('Inner Class Constructor')
        def displayS(self):
            print("Student Method")
        
    def displayC(self):
        print('Collage Method')

C = College()
C.displayC()
S = C.Student()
S.displayS()

R = College().Student()
R.displayS()