class Student:
    '''This is version 1.0'''

    def __init__(self, name, roll, marks):
        self.name = name
        self.roll = roll
        self.marks = marks
    
    def __str__(self):
        return  "This is Student Class"

    def display(self):
        print("Student Name: ", self.name)
        print("Roll Number:", self.roll)
        print("Marks:",self.marks)

'''
S = Student()
print(S.name)
print(S.roll)
print(S.marks)
print(S.__doc__)
print(S)
print('')
S.display()
'''
# List of objects
S= [
    Student('aaa',101,60.66),
    Student('bbb',102,70.0),
    Student('ccc',103,90.40)]

for i in S:
    i.display()
'''
print('')
S1.display()
S2.display()
S3.display()
'''