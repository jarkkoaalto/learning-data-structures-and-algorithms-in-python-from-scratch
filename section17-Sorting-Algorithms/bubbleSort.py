def bubblesort(A):
    for i in range(len(A)-1, 0, -1):
        for j in range(i):
            if A[j] > A[j+1]:
                A[j], A[j+1] = A[j+1], A[j]

A = [76,34,6,1,35,77]
print("Original Array", A)
bubblesort(A)
print("Sorted Array", A)