A = [34,78,4,6,97,33]
print("Original List", A)
A.sort()
print("Sorted List", A)

A = [34,78,4,6,97,33]
print("Original List",A)
B = sorted(A)
print("Sorted List",B)

A = [34,78,4,6,97,33]
print("Original List",A)
A.sort(reverse=True)
print("Sorted List",A)

B = ['red','green','blue','white']
print("Original List",B)
B.sort()
print("Sorted List",B)

B = ['red','green','blue','white']
print("Original List",B)
B.sort(reverse=True)
print("Sorted List",B)