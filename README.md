# Learning Data Structures and Algorithms in Python from Scratch #

Course outcomes - What do you get?
- Understanding of Data Structures and Algorithms
- Implementation in Python
- Help in facing interviews

Course is design for
- student 
- professional 
- self taught developers
- Student community

### Course Curriclum ###
- Why data structures?
- Analysis fo Algorithms ie Time and Space complexity
- Recursion
- Stacks, Queues and Deques
- Linked List
- Trees, Binary trees and Binary search trees
- Priority queues and Heaps
- Graphs, graph traversal algorithms
- Searching and Sorting algorithms

## Why Data Structures? ##
### Lets define DATA ###
- Data is some useful information
- why do we need data structures
- Why do we need to study internal structure of data?
- Why is it important to implement sata structures?

- Data can be stored in the memory in several way
- Using data structures we can store data effeciently
- Data structures is a way of arranging the data efficeintly in the memory
- Applications of programs that use this data can access it efficintly
- Applications of programs contain algorithms
- Algorithms are set of instrucions or rules that perform some task or operations in finite amount of time
- Data structures is a systematic way of organizing and accessing data

### Categories of data structures ###

Linear data structures: Stack, Queues, deques and linked list

Non-linear data structures: trees, heaps and graphs

## Lists ##
- List are an ordered sequence of data 
- List are mutable
- List are created using elements separated by commas within squares brackets L=[1,3,2,1,2,5,3,2]
- List can have elements of same type or mixed type

## Dictionaries ##
- Dictionaries have keys and values
- Instead of indices we use keys
- Key are immutable and unique
- Dictionaries are defined using curly brackets
- Each key, value pair is separated by comma

## Functions ##

- Function is a block of code
- Or a piece of code
- Runs only when it is called

## Build in modules ##
- Math
- Random
- Time
- Threading

- import module-name

## Creating own modules ##
Modules Advantages
- Allows large programs to be broken into manageable size
- Effective software development process
- Eventually integrated into complex complete system
- Facilitates program modification and updation.

## Exception handling ##
-Various errors may be generated at runtime
- When exception occurs the python program therminates
- Exceptions can be handling by program for smooth continuation of execution
- Exception is a Object in Python

standerd exceptions
- ImportError
- IndexError
- NameError
- TypeError
- ValueError
- IOError

## Object Oriended programming ##

Classes:

- Class is: data attributes and methods
- Class Box 
1. Data Attributes:
- lenght
- breadth
- height
2. Methods
- value
- area

### OOP Fundamental features ###
- Encapsulation
- Polymorphism
- Inheritance

## Linear search algorithm 
A=[84,21,47,96,14]
    0  1  2  3  4

## Algorithm ##

### Recursion ###
- Loops are used to executed repetitive statements
- Such as while and for loop in Python
- An etirely different way to achieve execution of repetitive statements is through Recursion
- Recursion is a technique where a function or method makes a call to itself
- Example:
1. Factorial Function (Classic mathematical function)
2. Fibonacci series (Sequence of Numbers)
3. Binary Search (Important search algorithm)
- Base case: there should be a condition through which no recursive call is made, this value is known as base case
- To implement recursion their should be at least one base case
- The chain of recursive calls should eventually reach the base case

Factorial(n) = n * Factorial(n-1)


## Introductiom to Data Structures ##

Alogrithm Analysis

- Consepts of Data structurs and Algorithms are center to computing
- We need an analysis tool to measure "goodness" of the Data Structures Operations and Algorithms

Time and Space complexity
- Time Complexity, Amount of time it takes to run an Algorithm
- Space Complexity: Amount of Memory used by the Algorithm

There may be several ways of computing a task, for example searching or sorting

Time Complexity - Experimental Analysis
- Running time of an algorithm can be measured by executing it on various inputs
- There byy recoring the time spent on each execution
- Python and several other programming languages provide time function
- Elapse time can be completely by recording the start and end of algorithm

Disadvantages
- xperiments should be performed in the same hardware and software environments
- Expetiments can be dome only on limited inputs

Time complexity - Theoretical Analysis
- Analysis is performed directly on description of the algorithm or actual program of function / operation
- Independent of hardware and software environments
- Takes all possible inputs

```python
def linearsearch(A,key):
    n = len(A)
    position = 0
    flag = False
    for position in range(n):
        if A[position]==key:
            flag = True
            return flag
        else:
            position = position + 1
    return flag
```

- Execution time of primitive operations is constant
- Assignment
- Performing Aritmetic operations
- Comparison statements
- Accessing elements
- Calling functions
- Returning functions

mathemetical function: f(n) = 6n + 4 = O(n)

A = 4,21,16,30,5 and S=4,5,16,21,30

```python
def partialsum(A):
    n = len(A)
    S = [0]*n
    for i in range(n):
        sum = 0
        for j in range(i+1):
            sum = sum + A[j]
        S[i] = sum
    return S
```

O(n^2)

f(n) = 2n^2 + 3n + 3

```
A = 4,21,16
B = 14,5,21
C = 21,8,7
```

```python
def disjoin(A,B,C):
for i in A:
    for j in B:
        for k in C:
            if i==j==k:
                return False
return True
```

Mathemetical Function: f (n) = 3 n^3 + n^2 + n + 1 = O(n^3)

```python

def binarysearch(A,key):
    first = 0
    last = len(A) - 1
    found = False
    while first <= last and not found:
        mid = (first+last)
        if A[mid] == key:
            found = True
        elif key < A[mid]:
            last = mid - 1
        elif key > A[mid]:
            first = mid + 1
    return found
```
O (log n)

### Abstract data type (ADT) ###
- Abstraction is one of the fundamental principles for Object Oriented Programming
- When Abstraction paradigm is applied to the Design of Data Structures, ADT's are formed
- ADT is mathematical model of Data Structures

ADT's specifies:
- Types of data stored
- Operations supported
- Parameters for the operations

ADT specify what each operation does

ADT does not specify how operations are done

## Stacks ##
- Stacks is a collection of objects
- Last-in first-out
- Fundamental operations are pushing and poping

### Stack Applications ###
- Web borowser history
- Undo operations is editing applications
- evaluating arithmetic expressions
- Parentheses matching
- HTML document tags matching
- Infix to postfix conversion
 
 ### Stack ADT ###
- Stack ADT stores objects
- Lifo scheme for Insert and delete operations
- Operations:
- push(object): insert element
- pop(): remove and return element
- top(): returns last insert elements
- len(): returns number of elements
- isEmpty(): whether stack is empty or not

## Queues ##

- Queue is a collection of objects
- First-in First-out
- Fundamental operations are  Enqueue and Dequeue

### Queue Applications ###
- Waiting queus 
Access to share opbjects

### Queues ADT ###
 queue ADT stors objets
 - Fifo scheme for insert and delete operations
 - Insertion at rear of queue
 - Removal at front of queue
 - Operations:
 - enqueue(objects): insert at end of the queue
 - dequeue(): remove and return at front of the queue
 - first(): returns elements at the front
 - len(): returns number of elements
 - isEmpty(): return is empty

## Deques abstract datatypes ##
- Deque ADT stores objects
- insert and delete operations at both ends (Front and last)
- Operations:
- add_first(object): insert at front of the deque
- add_last(object): insert at end of the deque
- delete_first(): remove fron fornt of the deque
- delete_last(): remove from rear of the deque
- first(): returns element at the front
- last(): return at the rear

## Linked list why we need? ##
- Problems with arrays
- array is of fixed size - immutable
- During runtime list size will be decided, we d not know how many elements ar compile time
- We need a data structure which should grow and shrinkk at runtime ie add nodes and delete nodes

Array vs linked list

Array -> 10 20 30 40 50

Linked List -> Node -> elements next => 10,null 20,null 30,null, 40,null = elements are linked other node => 10,20,30,40,50,null

Instance variables
- _elements
- _next

```python
class _Node:
     __slots__='element','_next'

def __init__(self, element,next):
    self._elements = elements
    self._next = next

n1 = _Node(10,None)
n1 = _Node(20,None)
```

Linked list Operations

Insertion:
- Insert Node at the beginning of the Linked List
- Insert Node at the end of the Linked List
- Insert Node at any position within the Linked list

Deletion:
- Remove node from the beginning of the linked list
- Remove node from the end of the Linked List
- Remove Node from any position within the Linked List

```python

def add_first(self,e):
    newest = self._Node(e, None)
    if self.is_empty():
        self._head = newest
        self._tail = newest
    else:
        newest._next = self._head
    self._head = newest
    self._size += 1
```

```python
def add_last(self,e):
    newest = self._Node(e,None)
    if self.is_empty():
        self._head = newest
        self._tail = newest
    else:
        self._tail._next = newest
    self._tail = newest
```

```python
def add_any(self,e,pos):
    newest = self._Node(e, None)
    threat = self._head
    i = 1
    while i<pos:
        thread = thread._next
        i=i+1
        newest._next = thread._next
        thread._next = newest
```

```python
def remove_first(self):
    if self.is_empty():
        raise Empty('List is empty')
    del_element = self._head._element
    self._head = self._head._next
    self._size -= 1
    if self.is_empty():
        self._head = None
        self._tail = None
```

```python
def remove_last(self):
    if self.is_empty():
        raise Empty('List is empty')
    thread = self_head
    i = 0
    while i < len(self)-2:
        thread = thread._next
        i=i+1
    self._tail = thread
    thread = thread._next
    del_element = thread._element
    self._tail._next = None
    self._size -= 1
```

```python
def remove_ any(self, pos):
    thread = self._head
    i=1
    while i < pos-1:
        thread = thread._next
        i = i+1
        thread._next = thread._next._next
        self._size -= 1
```

## Circular Linked List ##

Insertion:
- Insert node at the beginning of the Circular Linked List
- Insert Node at the end of the Circular Linked list
- Insert Node at any position within the Circular Linked List

Deletion:
- Remove Node from the beginning of the Circular Linked List
- Remove Node from the end of the Circular Linked List
- Remove Node from any position within the Circular Linked List

```python
def add_first(self, e):
    newest = self._Node(e, None)
    if self.is_empty():
        newest._next = newest
        self._head = newest
    else:
        self._tail._ next = newest
        newest._next = self._head
    self._head = newest
    self._size += 1
```

```python
def add_last(self,e):
    newest = self._Node(e, None)
    if self._is_empty():
        newest._next = newest
        self._head = newest
    else:
        newest._next = self._tail._next
        self._tail._next = newest
    self._tail = newest
```

```python
def add_any(self, e, pos):
    newest = self._Node(e,None)
    thead = self._head
    i=1
    while i<pos:
        thead = thead._next
        i=i+1
    newest._next = thead._next
    thead._next = newest
```

```python
def remove_first(self):
    if self.is_empty():
        raise Empty('List is empty')
    oldhead = self._tail._next
    self._tail_next = oldhead._next
    self._head = oldhead._next
    self._size -= 1
    if self.is_empty():
        self._tail = None
```

```python
def remove_last(self):
    if self.is_empty():
        raise Empty('List is empty')
    thead = self._head
    i=0
    while i<len(self)-2:
        thead = thead._next
        i=i+1
    self._tail = thead
    self._tail._next = self._head
    thead = thead._next
    ele_delete = thead._element
    self._size = 1
```

```python
def remove_any(self,pos):
    thead = self._theas
    i=1
    while i<pos-1:
        thead = thead._next
        i=i+1
        thead._next = thead._next._next
        self._size -= 1
```

## Double Linked List ##

Node 
- next
- element
- pref

Instanece Varialbles:
- _element
- _prev
- _next

```
class _Node:
    __slots__ = '_element','_prev','_next'
```


```
class _Node:
    __slots__ = '_element','_prev','_next'

    def __init__(self, element, prev, next):
        self._element = element
        self._prev = prev
        self._next = next
```

n1 = _Node(10,None, None)
- n1._element
- n1._prev
- n1._next

n1._next = n2

n2._prev = n1

Insertion:
- Insert node at the beginning of the Double Linked List
- Insert Node at the end of the Double Linked List
- Insert Node at any position within the Double Linked List

Deletion:
- Remove Node from the beginning of the double Linked LIst
- Remove Node from the end of the Double Linked List
- Remove Node from the any position within the Double Linked List


## Trees definition & properties ##

- Root
- Parent
- Child
- siblings
- Descendants
- Ancestors
- Internal Node
- Non-Lead Node
- External Node
- Leaf Node

- Degree of tree is maximum of nodes degrree
- Degree of Node is the number of children of the node
- Degree of leaf node is Zero

## Binary trees and it's properties ##
- Every Node has at most two children
- Every child node is labelled as left child or right child
- Left child precedes right child in order of Nodes

- Every binary tree with n Nodes has exaclty n-1 edges
- Full binary three (proper): if each Node has either two or Zero children

Complete Binary tree
- Complete binary thee: is a binary tree where nodes at each level are bumbered from left to right without any gap

Perfert binary tree:
- Perfect binary tree: is a binary tree whereall non-leaf nodes have two children and all leaf nodes are at same level

## Level order traversal of binary trees ##
Level order traversal: nodes are visited by level from top to bottom and within level nodes are visited from left to right

## Preorder traversal of binary tree ##
Pre order traversal: Root is visited first followed by left sub-tree and then right sub-tree

## Inorder Traversal of binary trees ##
In order traversal: left sub-tree is visited first, followed by root and then right sub-tree

## Postorder Traversal of binary trees ##
Post order traversal: Left sub-tree is visited first, followed by right sub-tree and the root


# Binary Search tree #

## Binary Search Tree Property ##

Binary search tree
- Every node has a key
- keys in left sub-tree of root are smaller than the key in the root
- Keys in rigth sub-tree of root are larger thanthe key in the root
- Left and right sub-trees are also binary search trees


## Priority Queues ##
- Queues collection of objects follows FIFO
- Exanmple: Customers waiting for service
- Many real applications FIFO policy does not suit
- Example: Air Traffic Control
- Through FIFO policy is reasonable, but some situations require objects to be priorized
- This is achieved using priority queues

### Heaps ###
- Heaps is a efficient realization of Priority queue
- Heap is a binary tree
- Relational property:
1. In a heap the value in each node is greater than or equal to those in its childern
- Structural property:
1. Heap is a complete binary tree
- Max heap and min  heap

### Heap absract data type ###
- Heap ADT stores prioritized objects
- Member.
1. MaxSize
2. CurrentSize
3. HeapList
- Operations:
1. insert(object): insert element
2. deleteMax(): remove & return element
3. max(): return root element
4. len(): returns numeber of elements
5. isEmpty(): whether stack is empty or not

## Graph Introduction ##

### Graphs definition and properties ###
- Graphs is a collection of object called as Vertices and together with repationship between them called as Edges
- Vertices = {A,B,C,D}
- Edges {A->B,A->D, B->C}

Edges is the grap are three types:
- Direct
- Undirect
- Weighted

- Directed: Edges has orientation A->B and B->A are not same
- Undirected: Edges has no orientation A-B or B-A are same
- Weighted: weights or cost are assigned to each edges

- End vertices or end points:
- IF edge is directed, first end point in origin and the other is the destination
- Outgoing edges: directed edges whose origin is that vertex
- Ingoming edges: diredted edges whose destination is that vertex
- Degree of vertex: Number of incident edges of the vertex
- In Degree of vertex: number of incoming edges of the vertex
- Out degree of vertex: number of outgoing edges of the vertex

- Path: is a sequence of edges starting at one vertex and ending at another vertex
- Cycle: is a path that start and end at same vertex

### Graphs ADT
- Create(n): create graph with n vertices and no edges
- Add(u,v) : Insert and edge from u to v
- Delete(u,v): remove an edge from u to v
- Edges() : returns number if edges
- Vertices(): returns numeees of vertices
- Exist(u,v): return true if edge between u and v exists
- Degree(u): returns degree of the vertex u
- InDegree(u): return in-degree of the vertex u
- OutDegree(u): return out-degree of the vertex u

### Graphs Traversal Algorithms ###

#### Graphs definition and Properties ####

- Graph traversal: is a technique or method of starting from a vertex and visiting all the vertices that can be reached form the start vertex

There are two efficient Graph Traveral algorithms:
- Breadth-first search
- Depth-first search

Breadth-first search (BFS)
- Start from a vertex and identify all vertices reachable from it
- Then start from the first reachable vertex, identifying all its reachable vertices, if the vertex is not yet visited
- Continue until all vertices are visited

Depth-first search (DFS)
- Start from a vertex and select the adjacent vertex from start vetex
- Visit the adjacent vertex and then repeat above step again, if adjacent vetex does not exist, the terminate the search

## Asymptotic Analysis

- Asymptotic: Approaching a value with some limits: f(n) n -> 1,2,3,4, ....
- Algorithms are analyzied using mathematical notations for functions which disregard constant factors
- Running time of algorithms are haracterized by using functions
- These functions are mapped with the size of the input n
- The running time of an algorithm grows proportinally to n.


