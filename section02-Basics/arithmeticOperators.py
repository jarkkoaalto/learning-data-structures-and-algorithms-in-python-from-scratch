"""

"""
# +.-.*,/
print(18+4)
print(18-4)
print(4-18)
print(18*4)
print(4.5-2)
print(4.5*2)

# Division and modulos operators 
print(10/2)
print(4.5/2)

print(14 % 4)
print(14.75%4)

# exponental
print(2**3)
print(33.44**2)

## Floor and ceiling operations
print(15//4) # = 3
print(15.5//4) # 3.0
print(-5//2) # -2.5  = -3
