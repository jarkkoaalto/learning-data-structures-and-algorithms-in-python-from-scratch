x = 10
y = 20
c = -5
u = "Hello"
o = "Python"
cx = 'H'
cy = 'P'

print(x < y)
print(x <= y)
print(x > y)
print(x >= y)
print(x >= c)
print(x <= c)

print(u>o)

print(ord(cx)) # 72 unicode
print(ord(cy)) # 80 unicode


v = 'aaa'
l = 'aab'
print(v < l) # True
print(v > l) # false

# Equality operators
f =10
k =20
print(f==k)
print(f!=k)