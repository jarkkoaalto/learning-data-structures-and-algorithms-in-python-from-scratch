#  datatypes the type of data stored in the memory
# 10 = integer
# 10.23 = float
# "hi" = string
# True = boolean

# data types in python: list, tuple, set, dict, forzenset, bytes, bytearray, range , None
import keyword

x  = 10
y = 10.24

print(type(x))
print(type(y))
print(type("hello"))

## Variables are user to storre values
my_var = 10

## Static vs dynamic type
'''
-Statically type :type of data has to be explicitly defined, such as in C, C++, Java
For example : int my_var = 10
- Dynamcally type: Type of data is determined at runtime
For example my_var=10
Python is a dynmically type programming language
'''

## Types: everything in python is object

print(keyword.kwlist)