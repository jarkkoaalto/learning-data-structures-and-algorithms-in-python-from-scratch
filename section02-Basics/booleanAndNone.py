'''
Boolean data type: true or false
'''

x = True
y = False

print(x)
print(type(x))
print(y)
print(type(y))


w = 10
n = 20
c = w > n
print(type(w))
print(type(n))
print(type(c))
print(c)

## None data type means on value associated. None is also and object in Python
u = 10
print(u)
print(type(u))
u = None
print(u)
print(type(u))
print(f)