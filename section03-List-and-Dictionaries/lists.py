L = [10,"hello", 2.85]
print(L)
L.insert(1,20)
print(L)
L.append(55.55)
print(L)
L.extend([56.6,'Python'])
print(L)

L[1] = "Hi"
print(L)

## Slicing
print(L[0:3])

print(L[3:5])

L2 = L[:]
print(L2)

my_str = 'Hello Python'
print(my_str.split())

my_str2 = '10,20,30,40,60'
print(my_str2.split(','))

my_list = [10,20,30,40,60]
print(my_list)
print(len(my_list))
print(min(my_list))
print(max(my_list))